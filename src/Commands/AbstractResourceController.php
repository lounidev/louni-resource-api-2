<?php

namespace Louni\DevResourceApi\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class AbstractResourceController extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:resource-abstract-controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create abstract resource controller';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = 'AbstractResourceController';
        $name = $this->qualifyClass("Http\\Controllers\\Api\\".$name);
        $path = $this->getPath($name);

        if(!file_exists($path)) {
            $this->makeDirectory($path);
            $this->files->put($path, $this->buildClass($name));
            $this->info('AbstractResourceController created successfully.');
        }else {
            $this->info('AbstractResourceController already exists.');
        }


    }


    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        // TODO: Implement getStub() method.

        $stub = __DIR__.'/stubs/abstractresourcecontroller.resource.stub';

        return $stub;

    }


    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

}
