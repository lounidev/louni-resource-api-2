<?php

namespace Louni\DevResourceApi;

use Illuminate\Support\ServiceProvider;

class ResourceCommandServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Louni\DevResourceApi\Commands\AbstractResourceController',
        'Louni\DevResourceApi\Commands\ApiResourceCommand',
        'Louni\DevResourceApi\Commands\ApiResourceController',
        'Louni\DevResourceApi\Commands\ApiResourceRepository',
        'Louni\DevResourceApi\Commands\ApiRequestRules',
        'Louni\DevResourceApi\Commands\ApiRequestFilters',
    ];
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
